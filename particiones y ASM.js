
command:

resp1=""
resp1="<INICIO>\n`lsblk --output=NAME,size,TYPE,MOUNTPOINT -n -r | awk '{print $1","$2","$3","$4"."}'  `\n<FIN>\n<RAM>\n`cat /proc/meminfo |grep MemTotal|awk '{print $2}'`\n<ENDRAM>\n<INICIO>\n`cat /etc/fstab`\n<FIN>\n<NAME>\n`uname -n`\n<NAMELESS>"
echo -e "${resp1}"

----------------------------------------------------------------------------------------------
Filtro de particiones tamaño y tipo 
----------------------------------------------------------------------------------------------
scriptlet:


var thatPlace = "";
var otherPlace = "";
var some = "";
var some2 = "";
// Recover RAM
thatPlace = scriptletInput.indexOf("echo -e");
thatPlace = scriptletInput.indexOf("<RAM>",thatPlace);
otherPlace = scriptletInput.indexOf("<ENDRAM>",thatPlace);
RAM = scriptletInput.substr(thatPlace+6,otherPlace-thatPlace-7);
if(0<RAM<9999&&RAM!="")
  RAM=Math.round(RAM/1024/1024);
else
{
  RAM=0;
  some += "Error al recuperar la RAM , se utilizara 0 como tamaño de SWAP y /var/crash\n";
}
// Recover Name
thatPlace = scriptletInput.indexOf("echo -e");
thatPlace = scriptletInput.indexOf("<NAME>",thatPlace);
otherPlace = scriptletInput.indexOf("<NAMELESS>",thatPlace);
hostName = scriptletInput.substr(thatPlace+7,otherPlace-thatPlace-8);
if(hostName=="")
  some += "El nombe del equipo no pudo ser recuperado , no se comprobaran los discos de base de datos.\n";
// Recover list of disk
thatPlace = scriptletInput.indexOf("echo -e");
thatPlace = scriptletInput.indexOf("<INICIO>",thatPlace);
otherPlace = scriptletInput.indexOf("<FIN>",thatPlace);
some2 = scriptletInput.substr(thatPlace+9,otherPlace-thatPlace-10);
if(some2=="")
  some += "No se recuperaron datos sobre las particiones , revisar permisos.\n";
else
{
  temp = some2.split(".\n");
  some3 = new Array(temp.length);
  temp2="";
  thatPlace = scriptletInput.indexOf("<INICIO>",otherPlace);
  otherPlace = scriptletInput.indexOf("<FIN>",thatPlace);
  stabber = scriptletInput.substr(thatPlace+9,otherPlace-thatPlace-10);
  if(stabber=="")
    some += "No se recupero la info del archivo /etc/fstab.\n";
  for (i=0;i<temp.length;i++)
  {
    some3[i] = temp[i].split(",");
    if(some3[i][0].substr(0,2)=="sd")
      temp2=some3[i][0];
    some3[i][4]=temp2;
  }  
  try
  {
    temp2=searchSub(temp.length,some3,0,5,"OS_vg");
    temp2=some3[temp2][4].substr(0,3);
    temp2=findDiv(temp.length,some3,0,temp2);
    some += isThisOfTheSize(80,some3[temp2][1],"el disco de SO");
    actPart=some3[temp2][0];
  }
  catch(error)
  {
    some += "Etiqueta OS_vg o disco SO no localizados\n";
    actPart = "error";
    delete error;
  }
  //disco1 part 1
  try
  {
    temp2 = findDiv(temp.length,some3,3,"/boot");
    some += disCheck(some3[temp2][4],actPart+"1","/boot");
    some += isThisOfTheSize(1,some3[temp2][1],"/boot");
    some += typePar("part",some3[temp2][2],"/boot");
  }
  catch(error)
  {
    some += "Particion /boot no localizada\n";
    actPart = "error";
    some += isThisOfTheSize(1,"0G","/boot");
    some += typePar("part","no","/boot");
    delete error;
  }
  //disco1 part 2
  checker1 = ["[SWAP]","/","/var","/opt","/home","/root/plataforma_OYM","/var/log","/var/log/audit","/usr","/tmp","/var/crash"];
  if(RAM<16)
    temp2=RAM;
  else
    temp2=16;
  checker2 = [temp2,5,6,5,5,3,3,1,5,5,RAM];
  for(runner=0;runner<checker1.length;runner++)
  {
    try
    {
      temp2 = findDiv(temp.length,some3,3,checker1[runner]);
      some += disCheck(some3[temp2][4],actPart+"2",checker1[runner]);
      some += isThisOfTheSize(checker2[runner],some3[temp2][1],checker1[runner]);
      some += typePar("lvm",some3[temp2][2],checker1[runner]);
      some += checkMount(some3[temp2][0],"OS",some3[temp2][3],stabber);
    }
    catch(error)
    {
      some += "Particion "+checker1[runner]+" no localizada\n";
      some += disCheck("no localizado",actPart+"2",checker1[runner]);
      some += isThisOfTheSize(1,"0G",checker1[runner]);
      some += typePar("lvm","no",checker1[runner]);
      some += checkMount("no localizado","OS",checker1[runner],stabber);
      delete error;
    }
  }
  //disco2
  try
  {
    temp2=searchSub(temp.length,some3,0,8,"OPENV_vg");
    temp2=some3[temp2][4].substr(0,3);
    temp2=findDiv(temp.length,some3,0,temp2);
    some += isThisOfTheSize(12,some3[temp2][1],"el disco de OPENV");
    actPart=some3[temp2][0];
  }
  catch(error)
  {
    some += "Etiqueta OPENV_vg o disco SO no localizados\n";
    actPart = "error";
    delete error;
  }
  //disco2 part 1
  checker1 = ["/usr/openv","/openview"];
  checker2 = [10,2];
  for(runner=0;runner<checker1.length;runner++)
  {
    try
    {
      temp2 = findDiv(temp.length,some3,3,checker1[runner]);
      some += disCheck(some3[temp2][4],actPart+"1",checker1[runner]);
      some += isThisOfTheSize(checker2[runner],some3[temp2][1],checker1[runner]);
      some += typePar("lvm",some3[temp2][2],checker1[runner]);
      some += checkMount(some3[temp2][0],"OPENV",some3[temp2][3],stabber);
    }
    catch(error)
    {
      some += "Particion "+checker1[runner]+" no localizada\n";
      some += disCheck("no localizado",actPart+"1",checker1[runner]);
      some += isThisOfTheSize(1,"0G",checker1[runner]);
      some += typePar("lvm","no",checker1[runner]);
      some += checkMount("no localizado","OPENV",checker1[runner],stabber);
      delete error;
    }
  }
  //base de datos?
  if(hostName.substr(-4,2).toLowerCase() == "bd")
  {
    // revisar si es /oracle o /mysql) 
    temp2="";
    try
    {
      temp2 = findDiv(temp.length,some3,3,"/oracle");
    }
    catch(error)
    {
      temp2="not oracle";
      delete error;
    }
    //  Es oracle
    if(temp2!="not oracle")
    {
      temp2=some3[temp2][4].substr(0,3);
      temp2=findDiv(temp.length,some3,0,temp2);
      some += isThisOfTheSize(210,some3[temp2][1],"el disco de ORA");
      actPart=some3[temp2][0];
      checker1 = ["/oraema","/rutinasbd","/descargasbd","/grid","/oracle"];
      checker2 = [3,3,100,50,50];
      for(runner=0;runner<checker1.length;runner++)
      {
        try
        {
          temp2 = findDiv(temp.length,some3,3,checker1[runner]);
          some += disCheck(some3[temp2][4],actPart+"1",checker1[runner]);
          some += isThisOfTheSize(checker2[runner],some3[temp2][1],checker1[runner]);
          some += typePar("lvm",some3[temp2][2],checker1[runner]);
          some += checkMount(some3[temp2][0],"ORA",some3[temp2][3],stabber);
        }
        catch(error)
        {
          some += "Particion "+checker1[runner]+" no localizada\n";
          some += disCheck("no localizado",actPart+"1",checker1[runner]);
          some += isThisOfTheSize(1,"0G",checker1[runner]);
          some += typePar("lvm","no",checker1[runner]);
          some += checkMount("no localizado","ORA",checker1[runner],stabber);
          delete error;
        }
      }
      actPart="";
      for(runner=0;runner<temp.length;runner++)
      {
        if(some3[runner][0].substr(0,6)=="ASM_vg"||some3[runner][0].substr(0,7)=="ACFS_vg")
        {
          if(some3[runner][4]!=actPart)
          {
            if(some3[runner][0].substr(0,6)=="ASM_vg")
            {
              some += "  -  Se detecto un disco ASM en la particion "+some3[runner][4]+"<br>\n";
              checker3="ASM_vg-asm";
            }
            else
            {
              some += "  -  Se detecto un disco ACFS en la particion "+some3[runner][4]+"<br>\n";
              checker3="ACFS_vg-acfs";
            }
            checker4="disk01_vol";
            if(some3[runner][0].substr(0,10)!=checker3||some3[runner][0].substr(-10,10)!=checker4)
              some += "     -  disco "+some3[runner][0]+" deberia llamarse "+checker3+checker4+"<br>\n";
            checker5=some3[runner][1].substr(0,some3[runner][1].length-1);
            if(some3[runner][2]!="lvm")
              some += "     -  disco "+some3[runner][0]+" es de un tipo diferente a lvm<br>\n"; 
            actPart=some3[runner][4];          
          }
          else
          {
            checker4 = Number(checker4.substr(4,2))+1;
            if(checker4<10)
              checker4 = "disk0"+checker4+"_vol";
            else
              checker4 = "disk"+checker4+"_vol";
            if(some3[runner][0].substr(0,10)!=checker3||some3[runner][0].substr(-10,10)!=checker4)
              some += "     -  disco "+some3[runner][0]+" deberia llamarse :"+checker3+checker4+"<br>\n";
            if(checker5!=some3[runner][1].substr(0,some3[runner][1].length-1))
              some += "     -  tamaño de disco = "+some3[runner][1].substr(0,some3[runner][1].length-1)+", deberia ser = "+checker5+"<br>\n";
            if(some3[runner][2]!="lvm")
              some += "     -  disco "+some3[runner][0]+" es de un tipo diferente a lvm<br>\n"; 
          }
        }
      }
    }
    //  Es mysql
    else
    {
      try
      {
        temp2 = findDiv(temp.length,some3,3,"/mysql");
      }
      catch(error)
      {
        temp2="not mysql";
        delete error;
      }
      if(temp2!="not mysql")
      {
        checker5=some3[temp2][0].indexOf("_vg");
        checker5=some3[temp2][0].substr(0,checker5);
        temp2=some3[temp2][4].substr(0,3);
        temp2=findDiv(temp.length,some3,0,temp2);
        some += isThisOfTheSize(50,some3[temp2][1],"el disco de "+checker5);
        actPart=some3[temp2][0];
        checker1 = ["/mysql","/backup","/mo1"];
        checker2 = [10,10,30];
        for(runner=0;runner<checker1.length;runner++)
        {
          try
          {
            temp2 = findDiv(temp.length,some3,3,checker1[runner]);
            some += disCheck(some3[temp2][4],actPart+"1",checker1[runner]);
            some += isThisOfTheSize(checker2[runner],some3[temp2][1],checker1[runner]);
            some += typePar("lvm",some3[temp2][2],checker1[runner]);
            some += checkMount(some3[temp2][0],checker5,some3[temp2][3],stabber);
          }
          catch(error)
          {
            some += "Particion "+checker1[runner]+" no localizada\n";
            some += disCheck("no localizado",actPart+"1",checker1[runner]);
            some += isThisOfTheSize(1,"0G",checker1[runner]);
            some += typePar("lvm","no",checker1[runner]);
            some += checkMount("no localizado",checker5,checker1[runner],stabber);
            delete error;
          }
        }
        actPart="";
        for(runner=0;runner<temp.length;runner++)
        {
          if(some3[runner][0].substr(0,6)=="APP_vg")
          {
            if(some3[runner][4]!=actPart)
            {
              some += "  -  Se detecto un disco filesystem en la particion "+some3[runner][4]+"<br>\n";
              checker3="APP_vg-app";
              checker4="disk01_vol";
              if(some3[runner][0].substr(0,10)!=checker3||some3[runner][0].substr(-10,10)!=checker4)
                some += "     -  disco "+some3[runner][0]+" deberia llamarse "+checker3+checker4+"<br>\n";
              checker5=some3[runner][1].substr(0,some3[runner][1].length-1);
              if(some3[runner][2]!="lvm")
                some += "     -  disco "+some3[runner][0]+" es de un tipo diferente a lvm<br>\n"; 
              actPart=some3[runner][4];
            }
            else
            {
              checker4 = Number(checker4.substr(4,2))+1;
              if(checker4<10)
                checker4 = "disk0"+checker4+"_vol";
              else
                checker4 = "disk"+checker4+"_vol";
              if(some3[runner][0].substr(0,10)!=checker3||some3[runner][0].substr(-10,10)!=checker4)
                some += "     -  disco "+some3[runner][0]+" deberia llamarse :"+checker3+checker4+"<br>\n";
              if(checker5!=some3[runner][1].substr(0,some3[runner][1].length-1))
                some += "     -  tamaño de disco = "+some3[runner][1].substr(0,some3[runner][1].length-1)+", deberia ser = "+checker5+"<br>\n";
              if(some3[runner][2]!="lvm")
                some += "     -  disco "+some3[runner][0]+" es de un tipo diferente a lvm<br>\n";   
            }          
          }
        }
      }
    }
  }
}
scriptletResult=some;

function findDiv(tam,mat1,place,cadena)
{
  for (i=0;i<tam;i++)
  {
    if(mat1[i][place]==cadena)
      return(i);
  }
}
function searchSub(tam,mat1,place,cant,cadena)
{
  for (i=0;i<tam;i++)
  {
    if(mat1[i][place].substr(0,cant)==cadena)
      return(i);
  }
}
function isThisOfTheSize(expect,given,name)
{
	if(given.substr(-1,1)=="G")
    given=given.substr(0,given.length-1)*1024;
  else
    given=given.substr(0,given.length-1);
  if(given<expect*1024 && name!="/var")
    ftemp="El tamaño de "+name+" <span style=\"color:#b02121;\"> NO ESTA DE ACUERDO CON EL ESTANDAR </span>.<br>\n"; 
  else
    ftemp="El tamaño de "+name+" <span style=\"color:#b02121;\"> CUMPLE CON EL ESTANDAR </span>.<br>\n";
	return(ftemp);
}
function disCheck(haveThis,needThis,name)
{
  ftemp="";
	if(haveThis!=needThis)
    ftemp="El disco en el que se encuentra "+name+" <span style=\"color:#b02121;\"> NO ESTA DE ACUERDO CON EL ESTANDAR </span>.<br>\n"; 
  else
    ftemp="El disco en el que se encuentra "+name+" <span style=\"color:#b02121;\"> CUMPLE CON EL ESTANDAR </span>.<br>\n";
  return(ftemp);
}
function typePar(haveThis,needThis,name)
{
  if(haveThis!=needThis)
    ftemp="El tipo de particion "+name+" <span style=\"color:#b02121;\"> NO ESTA DE ACUERDO CON EL ESTANDAR </span>.<br>\n"; 
  else
    ftemp="El tipo de particion "+name+" <span style=\"color:#b02121;\"> CUMPLE CON EL ESTANDAR </span>.<br>\n";
  return(ftemp);
}
function checkMount(haveThis,preNeed,name,fstab)
{
  needProc=preNeed+"_vg";
  if(name=="/")
    needProc += "-root_vol"
  else if(name=="[SWAP]")
    needProc += "-swap_vol"
  else
  {
    ftemp=name.substr(1,name.length-1);
    ftemp=ftemp.replace(/\//g,"_");
    needProc += "-"+ftemp+"_vol"
  }
  if(haveThis!=needProc)
    ftemp="El nombre de volumen de "+name+" <span style=\"color:#b02121;\"> NO ESTA DE ACUERDO CON EL ESTANDAR </span> es "+haveThis+" se esperaba "+needProc+" "; 
  else
    ftemp="El nombre de volumen "+name+" <span style=\"color:#b02121;\"> CUMPLE CON EL ESTANDAR </span>";
  if (fstab.includes("/dev/"+haveThis) == true)
    ftemp += "correctamente montado en fstab.<br>\n";
  else if (fstab.includes("/dev/mapper/"+haveThis) == true)
    ftemp += "montado en fstab (incluye mapper).<br>\n";
  else
    ftemp += "<span style=\"color:#b02121;\"> NO SE ENCUENTRA EN fstab </span>.<br>\n";
  return(ftemp);
}