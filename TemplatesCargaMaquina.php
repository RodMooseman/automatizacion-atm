#!/bin/bash
#
# Este script ejecuta las pruebas del ATP-OS para Linux y crea un archivo .html como resultado
#

#variables
NODO=`uname -n`
DATE=`date +%Y_%m_%d-%H-%M`
FILE="/tmp/ATP_OS_${NODO}_${DATE}.html"

echo "Iniciando pruebas de SO..."


#Creando el encabezado del archivo html
echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">" >> ${FILE}
echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"es\">" >> ${FILE}
echo "<head>" >> ${FILE} 

echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" >> ${FILE}
echo "<meta http-equiv=\"Content-Style-Type\" content=\"text/css\" />" >> ${FILE}

echo "<title> ATP_OS ${NODO} </title>" >> ${FILE}

#echo "<link rel=\"stylesheet\" href=\"atp.css\" title=\"ATP\" type=\"text/css\" media=\"screen,projection\" />" >> ${FILE}
echo "<link rel=\"stylesheet\" href=\"http://10.209.10.223/css_atp/atp.css\" title=\"ATP\" type=\"text/css\" media=\"screen,projection\" />" >> ${FILE}
echo "</head>" >> ${FILE}
echo "" >> ${FILE}

echo "<body>" >> ${FILE}
echo "" >> ${FILE}

echo "<div id=\"main\">" >> ${FILE}

echo "<div id=\"cabeza\">" >> ${FILE}
echo "<table class=\"primera\">" >> ${FILE}
echo "<tr><td class=\"logoT\"></td>" >> ${FILE}
echo "<td class=\"titulo\"><h1> ATP_OS ${NODO} </h1></td>" >> ${FILE}
echo "<td class=\"logoO\"></td></tr>" >> ${FILE}
echo "</table>" >> ${FILE}
echo "</div>" >> ${FILE}
echo "" >> ${FILE}

echo "<div id=\"menu\">" >> ${FILE}
echo "<ul>" >> ${FILE}
echo "<li><a href=\"#G1\" class=\"liga\">&raquo; Datos generales</a></li>" >> ${FILE}
echo "<li><a href=\"#T1\" class=\"liga\">&raquo; Identificación de Equipo</a></li>" >> ${FILE}
echo "<li><a href=\"#T2\" class=\"liga\">&raquo; Version del Sistema Operativo</a></li>" >> ${FILE}
echo "<li><a href=\"#T3\" class=\"liga\">&raquo; Parametros del BIOS</a></li>" >> ${FILE}
echo "<li><a href=\"#T4\" class=\"liga\">&raquo; Verificacion del Log Messages</a></li>" >> ${FILE}
echo "<li><a href=\"#T5\" class=\"liga\">&raquo; Verificacion del Procesador</a></li>" >> ${FILE}
echo "<li><a href=\"#T6\" class=\"liga\">&raquo; Verificacion de las interfaces de IP</a></li>" >> ${FILE}
echo "<li><a href=\"#T7\" class=\"liga\">&raquo; Tablas de Ruteo</a></li>" >> ${FILE}
echo "<li><a href=\"#T8\" class=\"liga\">&raquo; Conexion de puertos activos</a></li>" >> ${FILE}
echo "<li><a href=\"#T9\" class=\"liga\">&raquo; Revisión de recursos</a></li>" >> ${FILE}
echo "<li><a href=\"#T10\" class=\"liga\">&raquo; Integridad de Disco</a></li>" >> ${FILE}
echo "<li><a href=\"#T11\" class=\"liga\">&raquo; Validacion del archivo fstab</a></li>" >> ${FILE}
echo "<li><a href=\"#T12\" class=\"liga\">&raquo; Usuarios del Sistema</a></li>" >> ${FILE}
echo "<li><a href=\"#T13\" class=\"liga\">&raquo; Validacion de Kdump</a></li>" >> ${FILE}
echo "<li><a href=\"#T14\" class=\"liga\">&raquo; Paquetes Instalados</a></li>" >> ${FILE}
echo "<li><a href=\"#T15\" class=\"liga\">&raquo; Uso de memoria y paginacion</a></li>" >> ${FILE}
echo "<li><a href=\"#T16\" class=\"liga\">&raquo; Validacion de Zona Horaria</a></li>" >> ${FILE}
echo "<li><a href=\"#T17\" class=\"liga\">&raquo; Validacion de CRON</a></li>" >> ${FILE}
echo "<li><a href=\"#T18\" class=\"liga\">&raquo; Bitacora del sistema</a></li>" >> ${FILE}
echo "</ul>" >> ${FILE}
echo "</div>" >> ${FILE}
echo "" >> ${FILE}


echo "<div id=\"contenido\">" >> ${FILE}
#Datos generales
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"G1\" class=\"cont\">Datos generales</a></h2>" >> ${FILE}
echo "<p> Nombre del equipo </p>" >> ${FILE}
echo "<pre><code> uname -n: `uname -n` </code></pre>" >> ${FILE}
echo "<p> Fecha de ejecución de las pruebas</p>" >> ${FILE}
echo "<pre><code> `date +%H:%M_%d-%b-%Y` </code></pre>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV generales


#Prueba 1: <p></p><p></p>Identificacion de Equipo
echo "<div id=\"prueba\">" >> ${FILE}
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T1\" class=\"cont\">Identificación de Equipo</a></h2>" >> ${FILE}
echo "<p> Nombre del equipo </p>" >> ${FILE}
echo "<pre><code> uname -n: <p></p><p></p>`uname -n` </code></pre>" >> ${FILE}
echo "<p> Tipo de plataforma</p>" >> ${FILE}
echo "<pre><code> uname -i: <p></p>`uname -i` </code></pre>" >> ${FILE}
echo "<p> Tipo de procesador </p>" >> ${FILE}
echo "<pre><code> uname -p: <p></p>`uname -p` </code></pre>" >> ${FILE}
echo "<p> OS </p>" >> ${FILE}
echo "<pre><code> uname -s: <p></p>`uname -s` </code></pre>" >> ${FILE}
echo "<p> Kernel version </p>" >> ${FILE}
echo "<pre><code> uname -r: <p></p>`uname -r` </code></pre>" >> ${FILE}
echo "<p> Resumen </p>" >> ${FILE}
echo "<pre><code> uname -a: <p></p>`uname -a` </code></pre>" >> ${FILE}
echo "<p> HostID </p>" >> ${FILE}
echo "<pre><code> hostid: <p></p>`hostid` </code></pre>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba

#Prueba 2: <p></p>Version del Sistema Opearativo
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T2\" class=\"cont\"> Version del Sistema Opearativo </a></h2>" >> ${FILE}
echo "<p> Sistema Operativo </p>" >> ${FILE}
echo "<p><pre><code> cat /etc/*-release: <p></p>`cat /etc/*-release` </code></pre></p>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba

#Prueba 3: <p></p>Parametros del BIOS
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T3\" class=\"cont\">Parametros de Sistema y BIOS</a></h2>" >> ${FILE}
echo "<p> BIOS</p>" >> ${FILE}
echo "<p><pre><code> dmidecode -t bios: <p></p>`dmidecode -t bios` </code></pre></p>" >> ${FILE}
echo "<p> Sistema</p>" >> ${FILE}
echo "<p><pre><code> dmidecode -t system: <p></p>`dmidecode -t system` </code></pre></p>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba


#Prueba 4: <p></p>Verificacion del log messages
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T4\" class=\"cont\">Verificacion del log messages</a></h2>" >> ${FILE}
echo "<p> Errores </p>" >> ${FILE}
echo "<p><pre><code> dmesg | grep error: <p></p>`tail -5000 /var/log/messages | grep -i warning | wc -l`<p></p><pre class=\"caja\"><code>`tail -5000 /var/log/messages | grep -i warning` </code></pre></code></pre></p>" >> ${FILE}
echo "<p><pre><code> dmesg | grep error: <p></p>`tail -5000 /var/log/messages | grep -i error | wc -l`<p></p><pre class=\"caja\"><code>`tail -5000 /var/log/messages | grep -i error` </code></pre></code></pre></p>" >> ${FILE}
echo "<p><pre><code> dmesg | grep error: <p></p>`tail -5000 /var/log/messages | grep -i critical | wc -l`<p></p><pre class=\"caja\"><code>`tail -5000 /var/log/messages | grep -i critical` </code></pre></code></pre></p>" >> ${FILE}
echo "<p><pre><code> dmesg | grep error: <p></p>`tail -5000 /var/log/messages | grep -i fatal | wc -l`<p></p><pre class=\"caja\"><code>`tail -5000 /var/log/messages | grep -i fatal` </code></pre></code></pre></p>" >> ${FILE}
echo "<p><pre><code> dmesg | grep error: <p></p>`tail -5000 /var/log/messages | grep -i fault | grep -iv default | wc -l`<p></p><pre class=\"caja\"><code>`tail -5000 /var/log/messages | grep -i fault | grep -iv default | wc -l` </code></pre></code></pre></p>" >> ${FILE}

echo "<code> tail -500 /var/log/messages: </code> <pre class=\"caja\"><code>`tail -5000 /var/log/messages` </code></pre>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV Prueba

echo "<div class=\"prueba\">" >> ${FILE}
echo "<p> Busqueda de errores </p>" >> ${FILE}
echo "<p><pre><code> dmesg | grep error: <p></p>`dmesg | grep -i error | wc -l`<p></p><pre class=\"caja\"><code>`dmesg | grep -i error` </code></pre></code></pre></p>" >> ${FILE}
echo "<p><pre><code> dmesg | grep fail: <p></p>`dmesg | grep -i fail | wc -l`<p></p><pre class=\"caja\"><code>`dmesg | grep -i fail` </code></pre></code></pre></p>" >> ${FILE}
echo "<p><pre><code> dmesg | grep warning: <p></p>`dmesg | grep -i warning | wc -l`<p></p><pre class=\"caja\"><code>`dmesg | grep -i warning` </code></pre></code></pre></p>" >> ${FILE}
echo "<p><pre><code> dmesg | grep critical: <p></p>`dmesg | grep -i critical | wc -l`<p></p><pre class=\"caja\"><code>`dmesg | grep -i critical` </code></pre></code></pre></p>" >> ${FILE}
echo "<p><pre><code> dmesg | grep fault: <p></p>`dmesg | grep -i fault | wc -l`<p></p><pre class=\"caja\"><code>`dmesg | grep -i fault` </code></pre></code></pre></p>" >> ${FILE}
echo "<pre><code> dmesg: </code></pre> <pre class=\"caja\"><pre><code>`dmesg` </code></pre>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV Prueba


#Prueba 5: <p></p>Verificacion del Procesador
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T5\" class=\"cont\">Verificacion del Procesador</a></h2>" >> ${FILE}
echo "<p> CPU's </p>" >> ${FILE}
echo "<code> Físicos: `cat /proc/cpuinfo | grep -i 'physical id' | sort -u | wc -l` </code>" >> ${FILE}
echo "<code> Nucleos: `cat /proc/cpuinfo | grep -i 'core id' | sort -u | wc -l` </code>" >> ${FILE}
echo "<p> Informacion de CPU's </p>" >> ${FILE}
echo "<pre><code> cat /proc/cpuinfo: </code></pre> <pre class=\"caja\"><code>`cat /proc/cpuinfo` </code></pre>" >> ${FILE}
echo "<p> Reporte de Procesos </p>" >> ${FILE}
echo "<p><pre><code> mpstat -P ALL : <p></p><pre class=\"caja\"><code>`mpstat -P ALL` </code></pre></code></pre></p>" >> ${FILE}
echo "<h2><a name=\"T6\" class=\"cont\">Interfaces y Memoria</a></h2>" >> ${FILE}
echo "<p> Interfaces y Memoria </p>" >> ${FILE}
echo "<p><pre><code> free: <p></p>`free` </code></pre></p>" >> ${FILE}
echo "<code> Total de dispositivos: `lspci | wc -l`</code>" >> ${FILE}
echo "<p> Detalle de dispositivos: </p>" >> ${FILE}
echo "<pre class=\"caja\"><code>`lspci`</code></pre>" >> ${FILE}
echo "<p><pre><code> lspci | grep PCI | wc -l: <p></p>`lspci | grep PCI | wc -l` </code></pre></p>" >> ${FILE}
echo "<p><pre><code> lspci | grep PCI: <p></p><pre class=\"caja\"><code>`lspci | grep PCI ` </code></pre> </code></pre></p>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba



#Prueba 6: <p></p>Verificacion de las interfaces de IP
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T6\" class=\"cont\">Verificacion de las interfaces de IP</a></h2>" >> ${FILE}
echo "<p> Interfaces </p>" >> ${FILE}
echo "<p><pre><code> ifconfig -a: <p></p><pre class=\"caja\"><code>`ifconfig -a` </code></pre></code></pre></p>" >> ${FILE}
echo "<code> /etc/init.d/network status: </code><pre><code>`/etc/init.d/network status`</code></pre>" >> ${FILE}
echo "<code> /etc/init.d/network status: </code><pre><code>`systemctl status network`</code></pre>" >> ${FILE}
echo "<code><br /></code>" >> ${FILE}
echo "<code> ip addr show: </code><p></p><pre class=\"caja\"><code>`ip addr show`</code></pre>" >> ${FILE}
echo "<code><br /></code>" >> ${FILE}
echo "<p> Detalle de los archivos de Bonding: </p>" >> ${FILE}
echo "<table border=\"1\">" >> ${FILE}
echo "<tr><th>Archivo</th><th>Detalle</th></tr>" >> ${FILE}
for i in `ls -lh /proc/net/bonding/* | awk '{print $9}'`
do
echo "<tr><td><pre><code>${i}</code></pre></td><td><pre><code>`cat ${i}`</code></pre></td></tr>" >> ${FILE}
done
echo "</table>" >> ${FILE}
echo "<p> Detalle de interface: </p>" >> ${FILE}
echo "<table border=\"1\">" >> ${FILE}
echo "<tr><th><pre><code>interface</code></pre></th><th><pre><code>detalle</code></pre></th></tr>" >> ${FILE}
for i in `ifconfig -a | egrep -vi "RX|TX|collisions|txqueuelen|Metric|inet |inet6 |Interrupt|Loopback|^$" | awk '{print $1}'`
do
echo "<tr><td><pre><code>ethtool ${i} | grep Speed: </code></pre></td><td><pre><code>`ethtool ${i}`</code></pre></td></tr>" >> ${FILE}
done
echo "</table>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba

#Prueba 7: <p></p>Tablas de Ruteo
echo "<div class=\"prueba\">" >> ${FILE} 
echo "<h2><a name=\"T7\" class=\"cont\">Tabla de Ruteo</a></h2>" >> ${FILE}
echo "<p> Rutas</p>" >> ${FILE}
echo "<p><pre><code> route: <p></p> `route` </code></pre></p>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba


#Prueba 8: <p></p>Conexion de puertos activos
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T8\" class=\"cont\">Conexion de puertos activos</a></h2>" >> ${FILE}
echo "<p> Conecctions </p>" >> ${FILE}
echo "<p><pre><code> netstat -a | grep -i EST | wc -l: <p></p>`netstat -a | grep -i EST | wc -l` </code></pre></p>" >> ${FILE}
echo "<p><pre><code> netstat -a | grep -i TIME_ | wc -l: <p></p>`netstat -a | grep -i TIME_ | wc -l` </code></pre></p>" >> ${FILE}
echo "<p><pre><code> netstat -a | grep -i  CLOSE_ | wc -l: <p></p>`netstat -a | grep -i  CLOSE_ | wc -l` </code></pre></p>" >> ${FILE}
echo "<pre><code> netstat: </code></pre> <pre class=\"caja\"><pre><code>`netstat -a` </code></pre>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba


#Prueba 9: <p></p>Revisión de recursos
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T9\" class=\"cont\">Memoria SWAP</a></h2>" >> ${FILE}
echo "<p><pre><code> free -m: <p></p>`free -m` </code></pre></p>" >> ${FILE}
echo "<p><pre><code> cat /proc/swaps: <p></p>`cat /proc/swaps` </code></pre></p>" >> ${FILE}
echo "<h2><a name=\"T6\" class=\"cont\"> Consumo de recursos </a></h2>" >> ${FILE}
echo "<p>Procesadores</p>" >> ${FILE}
echo "<p>Estatus de los procesadores</p>" >> ${FILE}
echo "<code>mpstat -P ALL: </code><pre class=\"caja\"><code>`mpstat -P ALL`</code></pre>" >> ${FILE}
echo "<p>Procesos ejecutandose en el sistema </p>" >> ${FILE}
echo "<code>top -b -n 1: </code><pre class=\"caja\"><code>`top -b -n 1`</code></pre>" >> ${FILE}
echo "<p>Uso de los procesadores</p>" >> ${FILE}
echo "<code>sar -P ALL 1 10: </code><pre class=\"caja\"><code>`sar -P ALL 1 10`</code></pre>" >> ${FILE}
echo "<p>Memoria</p>" >> ${FILE}
echo "<p>Tamaño de paginación: </p>" >> ${FILE}
echo "<code>getconf PAGESIZE: `getconf PAGESIZE`</code>" >> ${FILE}
echo "<p>Estadísticas de la memoria virtual del sistema: </p>" >> ${FILE}
echo "<code>vmstat -w -S M 1 5: </code><pre class=\"caja\"><code>`vmstat -w -S M 1 5`</code></pre>" >> ${FILE}
echo "<p>Estadísticas de I/O en el sistema: </p>" >> ${FILE}
echo "<code>iostat -xm: </code><pre class=\"caja\"><code>`iostat -xm`</code></pre>" >> ${FILE}
echo "<p>Utilización de la memoria: </p>" >> ${FILE}
echo "<code>free -m: </code><pre class=\"caja\"><code>`free -m`</code></pre>" >> ${FILE}
echo "<p>Estadisticas de Memoria y Discos: </p>" >> ${FILE}
echo "<code>vmstat -w: </code><pre class=\"caja\"><code>`vmstat -w`</code></pre>" >> ${FILE}
echo "<code>vmstat -w -d: </code><pre class=\"caja\"><code>`vmstat -w -d`</code></pre>" >> ${FILE}
echo "<code><br /></code>" >> ${FILE}
echo "<code>vmstat -D: </code><pre class=\"caja\"><code>`vmstat -D`</code></pre>" >> ${FILE}
echo "<code><br /></code>" >> ${FILE}
echo "<p>Dispositivo SWAP configurado en el sistema: </p>" >> ${FILE}
echo "<code>swapon -s: </code><pre><code>`swapon -s`</code></pre>" >> ${FILE}
echo "<code>fdisk -l /dev/sda1: </code><pre><code>`fdisk -l /dev/sda1`</code></pre>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV Prueba


#Prueba 10: <p></p>Integridad de Disco
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T10\" class=\"cont\"> Configuración y espacio de los filesystems </a></h2>" >> ${FILE}
echo "<p> Filesystems Montados <code>`df -h | awk '{print $1}' | grep \"/dev\" | wc -l` </code> </p>" >> ${FILE}
echo "<p> Espacio del File System </p>" >> ${FILE}
echo "<p><pre><code> df -h: <p></p>`df -h` </code></pre>" >> ${FILE}
echo "<p><pre><code> fdisk -l: <p></p>`fdisk -l` </code></pre>" >> ${FILE}
echo "<p> Verificación de las particiones de los dispositivos físicos </p>" >> ${FILE}
echo "<p><code> fdisk -l: </code>" >> ${FILE}
echo "</p>" >> ${FILE}
echo "<p><pre><code> cat /proc/partitions: <p></p>`cat /proc/partitions` </code></pre>" >> ${FILE}
echo "<h2><a name=\"T15\" class=\"cont\">Integridad del Disco</a></h2>" >> ${FILE}
echo "<p> Disco </p>" >> ${FILE}
echo "<p><pre><code> lsblk : <p></p>`lsblk` </code></pre></p>" >> ${FILE}
echo "<p> Physical Volume </p>" >> ${FILE}
echo "<p><pre><code> pvscan : <p></p>`pvscan` </code></pre></p>" >> ${FILE}
echo "<p> Logical Volume </p>" >> ${FILE}
echo "<p><pre><code> lvs : <p></p>`lvs` </code></pre></p>" >> ${FILE}
echo "<p><pre><code> lvdisplay : <p></p>`lvdisplay` </code></pre></p>" >> ${FILE}
echo "<p> Volume Group </p>" >> ${FILE}
echo "<p><pre><code> vgs : <p></p>`vgs` </code></pre></p>" >> ${FILE}
echo "<p><pre><code> vgdisplay : <p></p>`vgdisplay` </code></pre></p>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba

#Prueba 11: <p></p><p></p>Validacion del archivo fstab
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T11\" class=\"cont\">Validacion del archivo fstab</a></h2>" >> ${FILE}
echo "<p> fstab </p>" >> ${FILE}
echo "<pre><code> cat /etc/fstab: <p></p><p></p> `cat /etc/fstab` </code></pre>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba


#Prueba 12: <p></p>Usuarios del Sistema
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T12\" class=\"cont\">Usuarios del Sistema</a></h2>" >> ${FILE}
echo "<p>Número de usuarios en el sistema: <code>`cat /etc/passwd | wc -l`</code></p>" >> ${FILE}
echo "<code>cat /etc/passwd: </code><pre class=\"caja\"><code>`cat /etc/passwd`</code></pre>" >> ${FILE}
echo "<p>Número de contraseñas en el sistema: <code>`cat /etc/shadow | wc -l`</code></p>" >> ${FILE}
echo "<code>cat /etc/shadow: </code><pre class=\"caja\"><code>`cat /etc/shadow`</code></pre>" >> ${FILE}
echo "<p>Número de grupos en el sistema: <code>`cat /etc/group | wc -l`</code></p>" >> ${FILE}
echo "<code>cat /etc/group: </code><pre class=\"caja\"><code>`cat /etc/group`</code></pre>" >> ${FILE}
echo "<table border=\"1\">" >> ${FILE}
echo "<p> Default sudo security policy module </p>" >> ${FILE}
echo "<p><pre><code> cat /etc/sudoers: <p></p><pre class=\"caja\"><code>`grep -v "^$\|^#" /etc/sudoers` </code></pre></code></pre></p>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba

#Prueba 13: <p></p>Validacion de Kdump
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T13\" class=\"cont\">Validacion Kdump</a></h2>" >> ${FILE}
echo "<p> Kdump status </p>" >> ${FILE}
echo "<p><pre><code> service kdump status : <p></p>`service kdump status` </code></pre></p>" >> ${FILE}
echo "<p><pre><code> systemctl status kdump: <p></p>`systemctl status kdump` </code></pre></p>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba


#Prueba 14: <p></p>Paquetes Instalados
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T14\" class=\"cont\">Paquetes Instalados</a></h2> ">> ${FILE}
echo "<p> Paquetes </p>" >> ${FILE}
echo "<p><pre><code> rpm -qa | wc -l: <p></p>`rpm -qa | wc -l`</code></pre></p>" >> ${FILE}
echo "<pre><code> rpm -qa: </code></pre> <pre class=\"caja\"><code>`rpm -qa` </code></pre>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba


#Prueba 15: <p></p>Uso de memoria y paginacion
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T15\" class=\"cont\">Uso de memoria y paginacion</a></h2>" >> ${FILE}
echo "<p> Procesos </p>" >> ${FILE}
echo "<p><pre><code> top -n1 : <p></p><pre class=\"caja\"><code>`top -n1` </code></pre> </code></pre></p>" >> ${FILE}
echo "<p> Paginacion </p>" >> ${FILE}
echo "<p><pre><code>sar -B 1 10: <p></p>`sar -B 1 10` </code></pre></p>" >> ${FILE}
echo "<p> Memoria, SWAP y CPU </p>" >> ${FILE}
echo "<p><pre><code> vmstat -w -S m 1 10 : <p></p>`vmstat -w -S m 1 10` </code></pre></p>" >> ${FILE}
echo "<p> Memoria </p>" >> ${FILE}
echo "<p><pre><code> free -m : <p></p>`free -m` </code></pre></p>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba


#Prueba 16: <p></p>Validacion de Zona Horaria
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T16\" class=\"cont\">Zona Horaria</a></h2>" >> ${FILE}
echo "<pre><code> ls -l /etc/localtime: <p></p>`ls -l /etc/localtime` </code></pre>" >> ${FILE}
echo "<p> NTP </p>" >> ${FILE}
echo "<pre><code> ntpq -pn: <p></p>`ntpq -pn` </code></pre>" >> ${FILE}
echo "<pre><code> ntpq -c ass: <p></p>`ntpq -c ass` </code></pre>" >> ${FILE}
echo "<pre><code> chronyc sources: <p></p>`chronyc sources` </code></pre>" >> ${FILE}
echo "<pre><code> /etc/ntp.conf: <p></p>`grep -v "^$\|^#"  /etc/ntp.conf` </code></pre>" >> ${FILE}

#Prueba 17: <p></p>Validacion de CRON
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T17\" class=\"cont\">Validacion de CRON</a></h2>" >> ${FILE}
echo "<p> Listado de las tareas de todos los usuarios: </p>" >> ${FILE}
echo "<table border=\"1\">" >> ${FILE}
echo "<tr><th><code>Usuario</code></th><th><pre><code>Tareas</code></pre></th></tr>" >> ${FILE}
for i in `cat /etc/passwd | awk '{print $1}' | cut -d: -f1`
do
echo "<tr><td><code>${i}</code></td><td><pre><code>`crontab -u ${i} -l `</code></pre></td></tr>" >> ${FILE}
done
echo "</table>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV generales


#Prueba 18: Bitacora del sistema.
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T18\" class=\"cont\">Banner</a></h2>" >> ${FILE}
echo "<pre><code> /etc/issue: <p></p>`cat /etc/issue` </code></pre>" >> ${FILE}
echo "<pre><code> /etc/issue.net: <p></p>`cat /etc/issue.net` </code></pre>" >> ${FILE}
echo "<pre><code> /etc/motd: <p></p>`cat /etc/motd` </code></pre>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba

#Prueba 19: <p></p>Integridad de servicios
echo "<div class=\"prueba\">" >> ${FILE}
echo "<h2><a name=\"T15\" class=\"cont\">Integridad de servicios</a></h2>" >> ${FILE}
echo "<p> Estatus </p>" >> ${FILE}
echo "<p><pre><code> systemctl status : <p></p><pre class=\"caja\"><code>`systemctl status` </code></pre> </code></pre></p>" >> ${FILE}
echo "<p> Detalles </p>" >> ${FILE}
echo "<p><pre><code> systemctl --failed : <p></p><pre class=\"caja\"><code>`systemctl --failed` </code></pre> </code></pre></p>" >> ${FILE}
echo "</div>" >> ${FILE} # Fin del DIV prueba


echo "</div>" >> ${FILE} # Fin del DIV Contenido
echo "" >> ${FILE}


# Fin del DIV main
echo "</div>" >> ${FILE}
echo "" >> ${FILE}
echo "</body>" >> ${FILE}
echo "</html>" >> ${FILE}



#Fin del script
echo ""
echo "Se ha finalizado la ejecucion de las pruebas."
echo ""
echo "Se ha generado el archivo ${FILE}"
echo ""


chown root:root /tmp/ATP_OS_${NODO}_*.html


