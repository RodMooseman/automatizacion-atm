
command:


lsblk --output=MOUNTPOINT,size,TYPE -n | awk '{print ":"$1","$2","$3"."}'


------------------------
Filtro de particiones tamaño y tipo 
------------------------
scriptlet:


var some = "";
var thatPlace ="";
var otherPlace ="";
var checker1 = ["/boot","[SWAP]","/,","/var","/home","/usr/openv","/openview","/root/plataforma_OYM"];
var checker2 = [1,8,12,10,10,8,3,3];
for(i=0;i<checker1.length;i++)
{
  //paso 1 recuperar particion
  thatPlace = scriptletInput.indexOf (checker1[i]);
  if(thatPlace==-1)
    some +="La partición "+checker1[i].replace(",","")+" no existe debido a esto: <br>\nEl tamaño de la partición "+checker1[i].replace(",","")+" <span style=\"color:#b02121;\">NO ESTA DE ACUERDO CON EL ESTANDAR</span>.<br>\nEl tipo de partición en "+checker1[i].replace(",","")+" <span style=\"color:#b02121;\">NO ESTA DE ACUERDO CON EL ESTANDAR</span>.<br>\n";
  else
  {
    otherPlace = scriptletInput.indexOf (".",thatPlace); 
    some2 = scriptletInput.substr(thatPlace+1,otherPlace-thatPlace-1).split(",");
    //paso 2 validar tamaño
    if(some2[1].substr(-1,1)=="G")
      some2[1]=some2[1].substr(0,some2[1].length-1)*1024;
    else
      some2[1]=some2[1].substr(0,some2[1].length-1);
    if(some2[1]<checker2[i]*1024 && checker1[i]!="/var")
      some += "El tamaño de la partición "+checker1[i].replace(",","")+" <span style=\"color:#b02121;\">NO ESTA DE ACUERDO CON EL ESTANDAR</span>.<br>\n"; 
    else 
    {
      if(checker1[i]=="/var")
      {
        thatPlace = scriptletInput.indexOf ("/var/log");
        otherPlace = scriptletInput.indexOf (".",thatPlace); 
        temp = scriptletInput.substr(thatPlace+1,otherPlace-thatPlace-1).split(",");
        if(temp[1].substr(-1,1)=="G")
          some2[1] = some2[1]+temp[1].substr(0,temp[1].length-1)*1024;
        else
          some2[1] = some2[1]+temp[1].substr(0,temp[1].length-1);
        if(temp[2]!="lvm")
          some += "El tipo de particion en /var/log es diferente a lvm.<br>\n";
        thatPlace = scriptletInput.indexOf ("/var/log/audit");
        otherPlace = scriptletInput.indexOf (".",thatPlace); 
        temp = scriptletInput.substr(thatPlace+1,otherPlace-thatPlace-1).split(",");
        if(temp[1].substr(-1,1)=="G")
          some2[1] = some2[1]+temp[1].substr(0,temp[1].length-1)*1024;
        else
          some2[1] = some2[1]+temp[1].substr(0,temp[1].length-1);
        if(temp[2]!="lvm")
          some += "El tipo de particion en /var/log/audit es diferente a lvm.<br>\n";
        if(some2[1]<checker2[i]*1024)
          some += "La suma de el tamaño de las particiones /var, /var/log y /var/log/audi <span style=\"color:#b02121;\">NO ESTA DE ACUERDO CON EL ESTANDAR</span>.<br>\n"; 
        else
          some += "La suma de el tamaño de las particiones /var, /var/log y /var/log/audi <span style=\"color:#b02121;\">CUMPLE CON EL ESTANDAR</span>.<br>\n";
      }
      else
        some += "El tamaño de la partición "+checker1[i].replace(",","")+" <span style=\"color:#2180b0;\">CUMPLE CON EL ESTANDAR</span>.<br>\n";
    }
    //paso 3 validar tipo
    if((some2[2]!="lvm"&&i>0)||(some2[2]=="lvm" && i==0))
      some += "El tipo de partición en "+checker1[i].replace(",","")+"<span style=\"color:#b02121;\"> NO ESTA DE ACUERDO CON EL ESTANDAR</span.<br>\n";
    else
      some += "El tipo de partición en "+checker1[i].replace(",","")+"<span style=\"color:#2180b0;\"> CUMPLE CON EL ESTANDAR</span>.<br>\n";
  }
}
scriptletResult = some;